FastQSP (Jack-o-Nine-Tails F95 Edition)
=======================================
This is the source code of the engine for the game "Jack-o-Nine-Tails". Specifically, for the version of the game found on F95Zone. Don't use it for other versions of Jack, it's not gonna work.

The only reason you'd want this would be to edit the engine of the game. If that's your intention, strap in 'cause it's gonna be a wild ride :D 

If you get stuck at any point feel free to shoot me a message on F95Zone @Lokplart.


Disclaimer
==========
This specific method for compiling this old engine has been found through a lot of trial and error by someone who had no idea what they were doing (Aka by me - Lokplart :D) and is possibly not the most direct way of doing it. As such some of the steps might make no sense to you if you're more experienced with this kind of stuff.

If you find a better, more direct way, please do message me :D


Resources for compiling
=======================
All of these are required. 
(THIS STEP CAN BE SKIPPED NOW) - Visual Studio 2013 --- https://my.visualstudio.com/Downloads?q=visual%20studio%202013&wt.mc_id=o\~msft\~vscom~older-downloads
- Visual Studio 2022 or 2019 (Choose the C++ Development package if prompted) --- https://visualstudio.microsoft.com/vs/
- VCPkg --- https://github.com/microsoft/vcpkg
- CMake Standalone GUI --- https://cmake.org/download/
- QT 5.4.2 (Download "qt-opensource-windows-x86-msvc2013-5.4.2") --- https://download.qt.io/new_archive/qt/5.4/5.4.2/
- The game itself (You only really need the code part but you should get the images too so you can have the game working properly and be able to test stuff) 
    - Code --- https://gitgud.io/jack-o-nine-tails/game-base
    - Images --- https://gitgud.io/jack-o-nine-tails/media/media-base


Setting up the tools
====================
(THIS STEP CAN BE SKIPPED NOW) - Visual Studio 2013 --- Just follow the installer. 
    - You'll need a Microsoft account to download it (I think).
    - The original engine was compiled using MSVC 2013 so that's why this is used.
- Visual Studio 2022 or 2019 --- Just install it as you would normally and make sure the C++ package that has CMake integration is installed.
    - This is where you'll edit the code. 
- VCPkg 
    - Download it from their Git.
    - Extract the archive somewhere on your PC.
    - Run "bootstrap-vcpkg.bat".
    - Open a CMD and navigate to the folder you installed VCPkg in.
    - Run this command: "vcpkg install oniguruma".
- CMake Standalone GUI --- Just follow the installer for CMake, it includes the CMake GUI.
- QT 5.4.2 --- Just follow the installer.


Building a Visual Studio solution using the CMake GUI.
===========================================
- "Where is the source code" --- The location of the code you downloaded/cloned from this repository.
- "Where to build the binaries" --- The location where CMake will build the solution.
- Press "Configure":
    - Generator --- Visual Studio 17 2022 (or Visual Studio 12 2013 if you are following the old process, marked with THIS STEP CAN BE SKIPPED NOW)
    - Optional Platform --- Win32 (make sure this is set to Win32)
    - Press "Finish"
- It will then throw an error telling you it didn't find "Oniguruma_DIR" 
    - Edit the Oniguruma_DIR path (double click to edit it) to "*VCPkg Directory*/installed/x86-windows/share/oniguruma"
    - Edit the QT5Core_DIR path (double click to edit it) to "*QT Directory*/QT5.4.2/5.4/msvc2013/lib/cmake/QT5" 
    - Press "Generate"
- It should now find all the other QT directories and build the solution to the path you specified. 

Congrats! You now have the same useless thing as when you started, just in a more complicated form. Exhilarating right?


Getting the game to run in debug mode in Visual Studio 2022 or 2019.
============================================================
- Open Visual Studios 2022 or 2019 and use it to open the solution (FastQSP.sln).
- On the top right (in the solution explorer) you will see 5 projects.
- Right click on "FastQsp (Visual Studio 2022 [2013 if you are following the old process, marked with THIS STEP CAN BE SKIPPED NOW])" and click "Set as Startup Project".
(THIS STEP CAN BE SKIPPED NOW) - Right click on "FastQsp (Visual Studio 2022 [2013 if you are following the old process, marked with THIS STEP CAN BE SKIPPED NOW])" and click "Properties"
(THIS STEP CAN BE SKIPPED NOW)    - C/C++ ---> Preprocessor
(THIS STEP CAN BE SKIPPED NOW)    - In "Preprocessor Definitions" find *GIT_VERSION=*. Right now that variable receives no value which is bad. So just assign it an empty string (*GIT_VERSION=""*). 
(THIS STEP CAN BE SKIPPED NOW)    - Click "Apply" and close the window.
- Now run the app by clicking on "Local Windows Debugger". It will throw you an error about a QT .dll file. Now we need to deploy QT.
- Go to "*Solution Directory*/gui/Debug" and copy the path to this directory in your clipboard.
- Press the Windows key and search for "Qt 5.4 32-bit for Desktop (MSVC 2013)" --- Open it.
    - Type: windeployqt --debug "*Path you copied earlier*" --- Make sure the path in enclosed in quotation marks.
    - Example command on my PC: windeployqt --debug "X:\Jack\jack\_engine_cmake\gui\Debug\FastQsp.exe"
- Go to "*VCPkg Directory*/installed/x86-windows/bin" and copy "onig.dll" to the same folder you just deployed QT to.
- Now, FastQSP will open and you can click on "File" in the top left and navigate to the .qsp file from the game and open it. 

Now, NOW you finally have a working version (Hopefully :D). 


Editing the files.
==================
The files you want to edit are:
- The GUI headers and sources under the "FastQsp" project
- The QSP headers and sources under the "qsp" project

Also, just in case you don't know, editing the solution in Visual Studio 2022 or 2019 will also edit the files you downloaded/cloned from this repository. So if you want to save your edit those are the files you need to backup. 

Also #2, the game will run a bit slower when started from Visual Studio. So don't worry about delay or lag. It's probably not something you did.  


Building a standalone release.
==============================
If you get to this point I highly suggest you reach out to us on the Jack-o-Nine-Tails thread over on F95Zone. You're more then welcome to come in and describe your changes/improvements and, if it's good work , more often then not we'll add it straight into the main game.

However if you don't care about that, then here's how you build a release version:

Just switching to Release mode in VS2022 or 2019 ain't gonna cut it because some files that CMake created when you built the solution don't change by editing in VS2022 or 2019. 
- First, make another solution from the source code (which is now changed cause you edited it). 
- Open the new solution in VS2022 or 2019. And switch from "Debug" to "Release" mode. (Left of the button you clicked to run the debug solution earlier)
(THIS STEP CAN BE SKIPPED NOW) - Set the "FastQsp (Visual Studio 2022 [2013 if you are following the old process, marked with THIS STEP CAN BE SKIPPED NOW])" project as startup and set the "GIT_VERSION" to an empty string again (Just like last time).
- Go to "*Solution Directory*/gui/Release" and copy the path to this directory in your clipboard.
- Press the Windows key and search for "Qt 5.4 32-bit for Desktop (MSVC 2013)" --- Open it.
    - Type: windeployqt --release "*Path you copied earlier*" --- Make sure the path in enclosed in quotation marks.
- Go to "*VCPkg Directory*/installed/x86-windows/bin" and copy "onig.dll" to the same folder you just deployed QT to.
- Run the game once in "Release" mode. 
- Now, from the Release folder, the files you need are:
    - FastQsp.exp
    - FastQsp.lib
    - FastQsp.exe --- Rename this one to "jack.exe"
    - onig.dll
- After you've copied the files you need, you can delete the release solution because you'll need to re-build it with CMake every time you want to make a release version.
    - Now you can copy them to the engine folder (Replace when prompted) or archive and post them.

As a general rule, only edit and test in debug mode. Then, when everything works, build the release version, get your files and delete the rest. 

---

A tip I could give you would be to be careful when you search what things do. This version of QT is very old, to the point that a few of the libraries used are deprecated now. So when you search a function for example, pay attention to when it was added and things like that.

Other then that, good luck and happy coding :D